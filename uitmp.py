# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'lazy.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(789, 440)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.mainTextEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.mainTextEdit.setGeometry(QtCore.QRect(10, 10, 661, 361))
        self.mainTextEdit.setPlainText("")
        self.mainTextEdit.setPlaceholderText("")
        self.mainTextEdit.setObjectName("mainTextEdit")
        self.netButton = QtWidgets.QPushButton(self.centralwidget)
        self.netButton.setGeometry(QtCore.QRect(690, 20, 93, 28))
        self.netButton.setObjectName("netButton")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 789, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Lazy工具箱"))
        self.netButton.setText(_translate("MainWindow", "网络信息"))
