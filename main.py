#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import QApplication, QMainWindow
from MainWindow import *



class UIWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(UIWindow, self).__init__(parent)
        self.setupUi(self)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = UIWindow()
    window.show()
    sys.exit(app.exec_())